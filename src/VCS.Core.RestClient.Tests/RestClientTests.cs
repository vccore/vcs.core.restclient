using System;
using System.Threading.Tasks;
using VCS.Core.RestClient.RestClient.Extensions;
using Xunit;

namespace VCS.Core.RestClient.Tests
{
    public class RestClientTests
    {
        [Fact]
        public async Task PostRequest()
        {
            var restClient = new RestClient.RestClient();
            
            var request = new PersonRequest();

            var result = await restClient
                .PostAsync("someurl")
                .Headers(headers =>
                {
                    headers.Add("My-Header", "header-value");
                })
                .Body(request)
                .As<PersonResponse>();
        }
    }
}