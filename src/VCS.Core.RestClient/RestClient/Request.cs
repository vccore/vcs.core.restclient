using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using VCS.Core.RestClient.RestClient.Interfaces;

namespace VCS.Core.RestClient.RestClient
{
    internal sealed class Request : IRequest
    {
        private readonly RestClient _client;
        private readonly HttpMessageInvoker _invoker;
        private readonly HttpRequestMessage _request;

        public Request(
            RestClient client,
            HttpMessageInvoker invoker,
            HttpRequestMessage request,
            CancellationToken cancellationToken)
        {
            _client = client;
            _invoker = invoker;
            _request = request;
            CancellationToken = cancellationToken;
        }

        public MediaTypeFormatter Formatter => _client.Formatter;

        public TaskAwaiter<IResponse> GetAwaiter()
        {
            return InvokeAsync().GetAwaiter();
        }

        public IRequest Uri(Uri requestUri)
        {
            _request.RequestUri = requestUri;
            return this;
        }

        public IRequest Parameters(Dictionary<string, string> parameters)
        {
            if (parameters?.Count != 0)
            {
                var paramString = string.Join("&", parameters.Select(p => $"{p.Key}={p.Value}"));
                
                _request.RequestUri = new Uri(_request.RequestUri, $"?{paramString}");
            }

            return this;
        }

        public IRequest Body<T>(T content, Encoding encoding = null, string mediaType = "application/json")
        {
            
            _request.Content = new StringContent(JsonConvert.SerializeObject(content), encoding ?? Encoding.UTF8, mediaType);
            return this;
        }
        
        public IRequest Body<T>(T content, JsonSerializerSettings serializerSettings, Encoding encoding = null, string mediaType = "application/json")
        {
            
            _request.Content = new StringContent(JsonConvert.SerializeObject(content, serializerSettings), encoding ?? Encoding.UTF8, mediaType);
            return this;
        }

        public IRequest Headers(Action<HttpRequestHeaders> headersAction)
        {
            headersAction(_request.Headers);
            return this;
        }

        public IRequest SetAuthToken(string token)
        {
            _request.Headers.Authorization = new AuthenticationHeaderValue("", token);
            return this;
        }

        public void Dispose()
        {
            _request.Dispose();
        }

        public CancellationToken CancellationToken { get; }

        private async Task<IResponse> InvokeAsync()
        {
            using (_request)
            {
                var response = await _invoker.SendAsync(_request, CancellationToken);

                return new Response(response, Formatter, CancellationToken);
            }
        }
    }
}