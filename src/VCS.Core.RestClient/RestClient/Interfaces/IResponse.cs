using System;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading;

namespace VCS.Core.RestClient.RestClient.Interfaces
{
    public interface IResponse : IDisposable
    {
        HttpResponseMessage Message { get; }
        MediaTypeFormatter Formatter { get; }
        CancellationToken CancellationToken { get; }
    }
}