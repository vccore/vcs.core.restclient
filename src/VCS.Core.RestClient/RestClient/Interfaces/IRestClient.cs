using System;
using System.Net.Http;
using System.Threading;

namespace VCS.Core.RestClient.RestClient.Interfaces
{
    public interface IRestClient
    {
        IRequest SendAsync(HttpMethod method, Uri requestUri, CancellationToken cancellationToken);
    }
}