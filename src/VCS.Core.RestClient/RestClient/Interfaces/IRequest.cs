using System;
using System.Collections.Generic;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using Newtonsoft.Json;

namespace VCS.Core.RestClient.RestClient.Interfaces
{
    public interface IRequest : IDisposable
    {
        MediaTypeFormatter Formatter { get; }

        CancellationToken CancellationToken { get; }

        TaskAwaiter<IResponse> GetAwaiter();

        IRequest Uri(Uri requestUri);

        IRequest Parameters(Dictionary<string, string> builder);

        IRequest Body<T>(T content, Encoding encoding = null, string mediaType = "application/json");
        IRequest Body<T>(T content, JsonSerializerSettings serializerSettings, Encoding encoding = null, string mediaType = "application/json");

        IRequest Headers(Action<HttpRequestHeaders> headersAction);
        IRequest SetAuthToken(string token);
    }
}