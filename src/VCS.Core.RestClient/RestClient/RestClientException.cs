using System;
using System.Net;

namespace VCS.Core.RestClient
{
    public class RestClientException : Exception
    {
        public RestClientException(HttpStatusCode statusCode, string errorBody)
            : base("Unsuccessfull request. Response code is: " + statusCode)
        {
            StatusCode = statusCode;
            ErrorBody = errorBody;
        }

        public string ErrorBody { get; set; }

        public HttpStatusCode StatusCode { get; }

        public static RestClientException CreateFromStatus(HttpStatusCode statusCode, string errorBody)
        {
            switch (statusCode)
            {
                case HttpStatusCode.BadRequest:
                    return new BadRequestClientException(errorBody);

                case HttpStatusCode.InternalServerError:
                    return new InternalServerErrorClientException(errorBody);

                case HttpStatusCode.NotFound:
                    return new NotFoundClientException(errorBody);

                case HttpStatusCode.Unauthorized:
                    return new UnauthorizedClientException(errorBody);

                default:
                    return new RestClientException(statusCode, errorBody);
            }
        }
    }

    public class BadRequestClientException : RestClientException
    {
        public BadRequestClientException(string errorBody)
            : base(HttpStatusCode.BadRequest, errorBody)
        {
        }
    }

    public class InternalServerErrorClientException : RestClientException
    {
        public InternalServerErrorClientException(string errorBody)
            : base(HttpStatusCode.InternalServerError, errorBody)
        {
        }
    }

    public class NotFoundClientException : RestClientException
    {
        public NotFoundClientException(string errorBody)
            : base(HttpStatusCode.NotFound, errorBody)
        {
        }
    }

    public class UnauthorizedClientException : RestClientException
    {
        public UnauthorizedClientException(string errorBody)
            : base(HttpStatusCode.Unauthorized, errorBody)
        {
        }
    }
}