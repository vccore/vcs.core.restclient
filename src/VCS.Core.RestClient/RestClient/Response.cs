using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading;
using VCS.Core.RestClient.RestClient.Interfaces;

namespace VCS.Core.RestClient.RestClient
{
    internal sealed class Response : IResponse
    {
        public Response(HttpResponseMessage message, MediaTypeFormatter formatter, CancellationToken cancellationToken)
        {
            Message = message;
            Formatter = formatter;
            CancellationToken = cancellationToken;
        }

        public HttpResponseMessage Message { get; }

        public MediaTypeFormatter Formatter { get; }

        public CancellationToken CancellationToken { get; }

        public void Dispose()
        {
            Message.Dispose();
        }
    }
}