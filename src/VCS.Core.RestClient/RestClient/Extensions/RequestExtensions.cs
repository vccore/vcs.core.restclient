﻿using System;
using System.Net.Http;
using System.Threading;
using VCS.Core.RestClient.RestClient.Interfaces;

namespace VCS.Core.RestClient.RestClient.Extensions
{
    public static class RequestExtensions
    {
        /// <summary>
        ///     GET request
        /// </summary>
        /// <param name="client">REST client</param>
        /// <param name="requestUri">Url</param>
        /// <returns>
        ///     <seealso cref="IRequest" />
        /// </returns>
        public static IRequest GetAsync(this IRestClient client, string requestUri)
        {
            return client.SendAsync(HttpMethod.Get, new Uri(requestUri), CancellationToken.None);
        }

        /// <summary>
        ///     POST request
        /// </summary>
        /// <param name="client">REST client</param>
        /// <param name="requestUri">Url</param>
        /// <returns>
        ///     <seealso cref="IRequest" />
        /// </returns>
        public static IRequest PostAsync(this IRestClient client, string requestUri)
        {
            return client.SendAsync(HttpMethod.Post, new Uri(requestUri), CancellationToken.None);
        }

        /// <summary>
        ///     PUT request
        /// </summary>
        /// <param name="client">REST client</param>
        /// <param name="requestUri">Url</param>
        /// <returns>
        ///     <seealso cref="IRequest" />
        /// </returns>
        public static IRequest PutAsync(this IRestClient client, string requestUri)
        {
            return client.SendAsync(HttpMethod.Put, new Uri(requestUri), CancellationToken.None);
        }

        /// <summary>
        ///     Delete request
        /// </summary>
        /// <param name="client">REST client</param>
        /// <param name="requestUri">Url</param>
        /// <returns>
        ///     <seealso cref="IRequest" />
        /// </returns>
        public static IRequest DeleteAsync(this IRestClient client, string requestUri)
        {
            return client.SendAsync(HttpMethod.Delete, new Uri(requestUri), CancellationToken.None);
        }
    }
}