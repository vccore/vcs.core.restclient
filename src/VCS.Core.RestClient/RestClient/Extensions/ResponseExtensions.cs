using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using VCS.Core.RestClient.RestClient.Interfaces;

namespace VCS.Core.RestClient.RestClient.Extensions
{
    public static class ResponseExtensions
    {
        /// <summary>
        ///     Getting typed response
        /// </summary>
        /// <param name="request">Request</param>
        /// <typeparam name="TValue">Response type</typeparam>
        /// <returns>Awaitable</returns>
        public static Task<TValue> As<TValue>(this IRequest request)
        {
            return request.ThrowIfError().As<TValue>();
        }

        /// <summary>
        ///     Getting typed response using custom json serializer settings
        /// </summary>
        /// <param name="request">Request</param>
        /// <param name="settings">Serializer settings</param>
        /// <typeparam name="TValue">Response type</typeparam>
        /// <returns>Awaitable</returns>
        public static Task<TValue> As<TValue>(this IRequest request, JsonSerializerSettings settings)
        {
            return request.ThrowIfError().As<TValue>(settings);
        }

        /// <summary>
        ///     Getting typed response using custom json converters
        /// </summary>
        /// <param name="request"></param>
        /// <param name="converters">JSON конвертеры</param>
        /// <typeparam name="TValue"></typeparam>
        /// <returns>Awaitable</returns>
        public static Task<TValue> As<TValue>(this IRequest request, params JsonConverter[] converters)
        {
            return request.ThrowIfError().As<TValue>(converters);
        }

        /// <summary>
        ///     Throw rest exception in case and instead of client exceptions
        /// </summary>
        /// <param name="task"></param>
        /// <returns></returns>
        /// <exception cref="RestClientException"></exception>
        public static async Task<IResponse> ThrowIfError(this IRequest task)
        {
            var response = await task;
            if (response.Message.IsSuccessStatusCode) return response;

            using (response)
            {
                var errorBody = response.Message.Content != null
                    ? await response.Message.Content.ReadAsStringAsync()
                    : null;
                throw RestClientException.CreateFromStatus(response.Message.StatusCode, errorBody);
            }
        }

        /// <summary>
        ///     Getting typed response
        /// </summary>
        /// <param name="responseTask"></param>
        /// <typeparam name="TValue">Response type</typeparam>
        /// <returns>Awaitable</returns>
        public static async Task<TValue> As<TValue>(this Task<IResponse> response)
        {
            using (var r = await response)
            {
                return r.Message.Content == null || r.Message.StatusCode == HttpStatusCode.NoContent
                    ? default(TValue)
                    : await r.Message.Content.ReadAsAsync<TValue>(
                        new[] {r.Formatter},
                        r.CancellationToken);
            }
        }

        /// <summary>
        ///     Getting typed response using custom json serializer settings
        /// </summary>
        /// <param name="responseTask"></param>
        /// <param name="settings">Настройки JSON сериализатора</param>
        /// <typeparam name="TValue">Тип возвращаемого запроса</typeparam>
        /// <returns>Awaitable</returns>
        public static async Task<TValue> As<TValue>(this Task<IResponse> response, JsonSerializerSettings settings)
        {
            using (var r = await response)
            {
                if (r.Message.Content == null || r.Message.StatusCode == HttpStatusCode.NoContent)
                    return default(TValue);

                var json = await r.Message.Content.ReadAsStringAsync();

                return JsonConvert.DeserializeObject<TValue>(json, settings);
            }
        }

        /// <summary>
        ///     Getting typed response using custom json converters
        /// </summary>
        /// <param name="responseTask"></param>
        /// <param name="converters">JSON конвертеры</param>
        /// <typeparam name="TValue"></typeparam>
        /// <returns>Awaitable</returns>
        public static async Task<TValue> As<TValue>(this Task<IResponse> response, params JsonConverter[] converters)
        {
            using (var r = await response)
            {
                if (r.Message.Content == null || r.Message.StatusCode == HttpStatusCode.NoContent)
                    return default(TValue);

                var json = await r.Message.Content.ReadAsStringAsync();

                return JsonConvert.DeserializeObject<TValue>(json, converters);
            }
        }
    }
}