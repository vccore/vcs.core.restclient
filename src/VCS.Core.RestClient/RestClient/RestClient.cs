﻿using System;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using VCS.Core.RestClient.RestClient.Interfaces;

namespace VCS.Core.RestClient.RestClient
{
    public sealed class RestClient : IRestClient, IDisposable
    {
        private bool _isDisposed = false;
        private readonly HttpMessageInvoker _client;

        public RestClient() : this(new HttpClientHandler())
        {
            
        }
        public RestClient(HttpMessageHandler handler = null, JsonSerializerSettings jsonSerializerSettings = null)
        {
            _client = new HttpMessageInvoker(handler ?? new HttpClientHandler());

            var settings = jsonSerializerSettings ?? new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
                NullValueHandling = NullValueHandling.Ignore,
                Converters = { new StringEnumConverter() }
            };
            
            Formatter = new JsonMediaTypeFormatter { SerializerSettings = settings };
        }

        public MediaTypeFormatter Formatter { get; set; }

        /// <summary>
        ///     Sending HTTP request
        /// </summary>
        /// <param name="method">HTTP method</param>
        /// <param name="requestUri">Url</param>
        /// <param name="cancellationToken">Cancellation token</param>
        /// <returns>
        ///     <seealso cref="IRequest" />
        /// </returns>
        public IRequest SendAsync(HttpMethod method, Uri requestUri, CancellationToken cancellationToken)
        {
            return new Request(this, _client, new HttpRequestMessage(method, requestUri), cancellationToken);
        }

        #region IDisposable implementation

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool isDisposing)
        {
            if (!_isDisposed)
            {
                if (isDisposing)
                {
                    _client?.Dispose();
                }

                _isDisposed = true;
            }
        }

        #endregion IDisposable implementation
    }
}